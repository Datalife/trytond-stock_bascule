# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.tests.test_tryton import suite as test_suite
from trytond.transaction import Transaction
from trytond.pool import Pool
from unittest.mock import patch


class StockBasculeTestCase(ModuleTestCase):
    """Test Stock Bascule module"""
    module = 'stock_bascule'

    @with_transaction()
    def test_bascule_measure(self):
        pool = Pool()
        Bascule = pool.get('stock.bascule')
        IrModel = pool.get('ir.model')

        model, = IrModel.search([
            ('model', '=', 'stock.move')])

        bascule = Bascule(name='Bascule 1',
            model=model, uri='tcp://0.0.0.0:6767')
        bascule.save()
        with patch('trytond.modules.stock_bascule.bascule.socket.socket'
                ) as mock_socket:
            mock_socket().__enter__().recv().decode.return_value = '560.0'
            self.assertEqual(bascule.read_measure(), 560.0)

    @with_transaction()
    def test_user_bascule_match(self):
        pool = Pool()
        User = pool.get('res.user')
        Bascule = pool.get('stock.bascule')
        UserBascule = pool.get('res.user-stock.bascule')
        IrModel = pool.get('ir.model')

        product_model, = IrModel.search([
            ('model', '=', 'product.product')])
        move_model, = IrModel.search([
            ('model', '=', 'stock.move')])
        bascule_product = Bascule(name='Bascule for product',
            model=product_model, uri='tcp://internal_ip1:5000')
        bascule_product.save()
        bascule_move = Bascule(name='Bascule for moves',
            model=move_model, uri='tcp://internal_ip2:5000')
        bascule_move.save()

        user = User(Transaction().user)
        user.bascules = [
            UserBascule(bascule=bascule_move, sequence=10),
            UserBascule(bascule=bascule_product, sequence=20)]
        user.save()

        pattern = {'model': 'product.product'}
        self.assertEqual(user.get_bascule(pattern), bascule_product)
        pattern = {'model': 'stock.move'}
        self.assertEqual(user.get_bascule(pattern), bascule_move)
        pattern = {'model': 'product.template'}
        self.assertIs(user.get_bascule(pattern), None)


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            StockBasculeTestCase))
    return suite
