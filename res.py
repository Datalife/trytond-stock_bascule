# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import (ModelView, ModelSQL, fields, sequence_ordered,
    MatchMixin)
from trytond.pool import PoolMeta

__all__ = ['User', 'UserBascule']


class User(metaclass=PoolMeta):
    __name__ = 'res.user'

    bascules = fields.One2Many('res.user-stock.bascule', 'user', 'Bascules')

    def get_bascule(self, pattern):
        pattern = pattern.copy()
        for bascule in self.bascules:
            if bascule.match(pattern):
                return bascule.bascule
        return None


class UserBascule(sequence_ordered(), ModelSQL, ModelView, MatchMixin):
    '''User Bascule'''
    __name__ = 'res.user-stock.bascule'
    tablename = 'res_user_bascule_rel'

    user = fields.Many2One('res.user', 'User', required=True)
    bascule = fields.Many2One('stock.bascule', 'Bascule', required=True)

    def match(self, pattern):
        pattern = pattern.copy()
        if 'model' in pattern:
            if self.bascule.model.model != pattern['model']:
                return False
            pattern.pop('model')
        return super().match(pattern)
