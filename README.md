datalife_stock_bascule
======================

The stock_bascule module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-stock_bascule/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-stock_bascule)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
