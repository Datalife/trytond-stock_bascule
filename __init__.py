# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import bascule
from . import res


def register():
    Pool.register(
        bascule.Bascule,
        res.User,
        res.UserBascule,
        module='stock_bascule', type_='model')
